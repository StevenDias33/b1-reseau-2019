# Cours
## Supports & TPs

* Cours
  * [1. Manipulation d'adresses IP](./1/README.md)
  * [2. Notion de ports, et routage](./2/README.md)
* [Lexique](./lexique.md)
* Mémos
  * [Mémo configuration réseau CentOS 7](./memo/linux_centos_network.md)
  * [Mémo commandes Linux](./memo/linux_commandes.md) (commandes génériques, et commandes spécifiques au réseau)

## Objectif du cours réseau B1

* appréhender les termes et protocoles de bases
  * *[IP](./lexique.md#ip-internet-protocol), [MAC](./lexique.md#mac-media-access-control), [routage](./lexique.md#routage-ou-routing), client/serveur, [DHCP](./lexique.md#dhcp-dynamic-host-configuration-protocol), [DNS](./lexique.md#dns-domain-name-system), [HTTP](./lexique.md#http-hypertext-transfer-protocol), etc.*
* maîtriser la [stack réseau](./lexique.md#stack-réseau-ou-stack-tcpip-ou-pile-réseau) de son propre poste (Windows, Linux ou Mac)
  * gestion de [cartes réseau](./lexique.md#carte-réseau-ou-interface-réseau)
  * gestion [IP](./lexique.md#ip-internet-protocol) ([DHCP](./lexique.md#dhcp-dynamic-host-configuration-protocol)/statique, [gateway](./lexique.md#passerelle-ou-gateway), [binaire](./lexique.md#binaire), [masque de sous-réseau](./lexique.md#masque-de-sous-réseau), etc.)
  * [firewall](./lexique.md#pare-feu-ou-firewall)
  * exploration des [ports](./lexique.md#ports) utilisés
  * [ligne de commande](./lexique.md#commandes)
* comprendre le rôle d'une Box internet et ses principales fonctionnalités : 
  * routeur WAN/LAN
  * AP WiFi
  * [DNS](./lexique.md#dns-domain-name-system)
  * [DHCP](./lexique.md#dhcp-dynamic-host-configuration-protocol)
  * NAT
  * [firewall](./lexique.md#pare-feu-ou-firewall)
* savoir (vite fait) gérer le réseau d'une distrib Linux orientée serveur
  * [CentOS 7](https://www.centos.org/)
  * gestion de [stack réseau](./lexique.md#stack-réseau-ou-stack-tcpip-ou-pile-réseau)
  * administration distante ([SSH](./lexique.md#ssh-secure-shell), [firewall](./lexique.md#pare-feu-ou-firewall))
  * [routage](./lexique.md#routage-ou-routing) statique
* appréhender le monde Cisco
  *  [routage](./lexique.md#routage-ou-routing) statique et dynamique
  * switching
  * appréhension de la première certification ([CCNA](https://www.cisco.com/c/en/us/training-events/training-certifications/certifications/associate/ccna-routing-switching.html))
* connaissances techniques en réseau
  * modèle OSI et principaux protocoles
  * Internet
