# Commandes

- [Commandes](#commandes)
  - [Commandes GNU/Linux de base](#commandes-gnulinux-de-base)
  - [`dhclient` (Linux only)](#dhclient-linux-only)
  - [`ipconfig` ou `ifconfig` ou `ip a`](#ipconfig-ou-ifconfig-ou-ip-a)
  - [`ping`](#ping)
  - [`nmap`](#nmap)
  - [`nc` ou `netcat`](#nc-ou-netcat)
  - [`netstat` ou `ss`](#netstat-ou-ss)
  - [`nslookup` ou `dig`](#nslookup-ou-dig)
  - [`curl` et `wget`](#curl-et-wget)
  - [`tcpdump`](#tcpdump)
  - [`traceroute`](#traceroute)

## Commandes GNU/Linux de base

Commande | Signification | Utilité | Exemple
--- | --- | --- | ---
`cd` | "Change directory" | Permet de se déplacer dans un répertoire donné | `cd /etc/sysconfig/` : se déplacer dans le dossier `/etc/sysconfig/`
`cat` | x | Permet d'afficher le contenu d'un fichier donné | `cat /etc/resolv.conf` : affiche le contenu du fichier `/etc/resolv.conf` 
`cp` | "Copy" | Copie/colle un fichier | `cp /etc/resolv.conf /tmp/toto` : copie le fichier `/etc/resolv.conf` et le colle dans le fichier `/tmp/toto` (il est créé s'il n'existe pas)
`mv` | "Move" | Coupe/colle un fichier | `mv /etc/resolv.conf /tmp/toto` : coupe le fichier `/etc/resolv.conf` et le colle dans le fichier `/tmp/toto` (il est créé s'il n'existe pas)
`nano` ou `vi` ou `vim` | x | Editeur de texte | `nano /etc/selinux/config` : ouvre le fichier `/etc/selinux/config` dans un éditeur de texte `nano`, pour le modifier

## `dhclient` (Linux only)
* permet de demander une IP en DHCP
* pour lâcher votre bail DHCP (`DHCPRELEASE`)
  * `sudo dhclient -v -r` (`r` comme `renew`)
* pour redemander une IP ([DORA](./6.md#dhcp))
  * `sudo dhclient -v`

## `ipconfig` ou `ifconfig` ou `ip a`
* affiche des informations sur les carte réseau
```
# Windows
ipconfig 
ipconfig /all

# GNU/Linux ou MacOS
ifconfig
ip a
```

## `ping`
* message très simple qui fait un aller-retour sur le réseau
* on l'utilise souvent pour tester la présence de quelqu'un sur le réseau
* la valeur en millisecondes est le temps de l'aller-retour
* utilisation : `ping IP` où `IP` est l'adresse IP d'un hôte sur le réseau
* test d'accès internet
  * souvent, pour tester l'accès d'une machine à internet on fait `ping 8.8.8.8`
  * `8.8.8.8` est une adresse simple  mémoriser et correspond en réalité à un serveur de Google
  * `1.1.1.1` peut aussi être utilisé de la même façon, c'est un serveur de CloudFlare
  
## `nmap`
* outil de scan réseau
* permet de récupérer des informations sur un réseau et les machines qui y sont connectées
* beaucoup de scans sont possibles
  * le plus simple, le scan de ping envoie simplement un `ping` à toutes les IPs d'un réseau
  * exemple de scan de ping : `nmap -sP 192.168.1.0/24`

## `nc` ou `netcat`
* outil permettant de simples connexions [TCP](#tcp-transmission-control-protocol) ou [UDP](#udp-user-datagram-protocol)
* on a effectué des simples connexions entre vos PCs en cours
* mais on peut aussi s'en servir pour se connecter à un serveur web, aucun problème
  * faut savoir parler l'[HTTP](#http-hypertext-transfer-protocol) par contre ehe
  
* pour agir comme un serveur (Linux) :
```
# Lance netcat pour qu'il "écoute" sur le port 9999 de l'IP 10.1.0.10
# -l comme "listen"
nc -l 10.1.0.10 9999
```
* pour agir comme un client, et se connecter à un serveur (Linux) : 
```
# Pour se connecter au serveur 10.1.0.10 sur le port 9999 
nc 10.1.0.10 9999
```

## `netstat` ou `ss`
* outils permettant de lister les connexions actives d'une machine
  * entre autres, une par site web que l'on visite par exemple
  * **on l'utilise beaucoup pour lister les [ports](#ports) ouverts par une machine**
* options communes de `ss`
  * `-l` pour les ports en écoute (`-l` comme *listen*)
  * `-t` pour les ports TCP
  * `-u` pour les ports UDP
  * `-4` pour les connexions IPv4
  * `-n` pour ne pas transformer le numéro de ports en nom de service
  * `-p` pour voir l'application (le processus) qui est attaché à un port

Exemples :

```
# Lister les connexions actives en TCP (-t), UDP (-u)
$ ss -t -u

# Liste les ports en écoute (-l), en TCP (-t) et UDP (-u)
$ ss -l -t -u

# Liste les ports en écoute, en TCP et UDP, sans traduire les numéros de protocole (-n)
$ ss -l -t -u -n

# Idem, mais affiche en plus le processus (le "programme", ou "logiciel", ou "application") qui est en jeu dans cette connexion (-p)
$ sudo ss -l -t -u -n -p
$ sudo ss -ltunp # plus rapide
```

## `nslookup` ou `dig`
* outils permettant d'effectuer des opérations liées au protocole DNS
* un *lookup DNS* consiste à demander quelle est l'IP d'un nom donné
  * "à quelle IP se trouve le serveur `www.google.com` ?" par exemple
* un *reverse lookup DNS* c'est l'inverse : on cherche à connaître à quel nom est associée une IP
  * "y'a-t-il des [noms de domaines](./4.md#noms-de-domaine) associés à `76.32.43.32` ?" par exemple

## `curl` et `wget`
* permettent de faire des requêtes [HTTP](#http-hypertext-transfer-protocol)
* tout ce que fait `wget`, `curl` sait le faire. La réciproque n'est pas vraie.
* `wget` c'est l'outil simple mais peu puissant et très peu flexible
* `curl`, c'est l'inverse
* souvent, pour imiter un simple `wget`, vous pouvez faire `curl -SLO`

Exemple :
* `curl -L www.google.com` permet de récupérer le contenu du serveur web de  `www.google.com`

## `tcpdump`
* permet de faire de simples captures réseaux 
* utilisation simpliste : `sudo tcpdump -i enp0s3 -w capture.pcap`
  * `-i` pour cibler une interface réseau spécifique
  * `-w` pour enregistrer la capture dans un fichier spécifique
  * `-n -nn` pour avoir des numéros de port plutôt que des noms
  * `.pcap` est l'extension standard pour les captures réseau

> **Une fois la capture lancée, on peut l'arrêter avec `CTRL + C`.**

Exemples :
```
# Intercepter et afficher le trafic de l'interface enp0s8
$ sudo tcpdump -n -nn -i enp0s8

# Intercepter et enregistrer le trafic de l'interface enp0s8 dans un fichier traffic.pcap
$ sudo tcpdump -n -nn -i enp0s8 -w traffic.pcap
```

> **Vous pouvez ouvrir un fichier `.pcap` dans Wireshark, pour une meilleure lisibilité.**

Vous pouvez aussi **filtrer le trafic capturé** avec des expressions logiques.

Par exemple, pour ne pas intercepter le trafic SSH (port 22) :
```
$ sudo tcpdump -n -nn -i enp0s8 not port 22
```

## `traceroute`
* permet d'afficher les machines intermédiaires pour aller à une destination
* `traceroute` a plusieurs méthodes de fonctionnement
  * la plus classique est d'envoyer plusieurs `ping`
* exemple : 
  * `traceroute <IP>`

