# Installation de CentOS7

Le document qui suit détaille l'installation de CentOS7 réalisée en cours.

Quelques détails généraux sur l'environnement : 
* CentOS7 est une distribution GNU/Linux (basée sur RedHat)
* les instructions au sujet de l'hyperviseur seront données pour **VirtualBox**

## Prérequis

* télécharger [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* télécharger le [.iso de CentOS **en version minimale**](http://isoredirect.centos.org/centos/7/isos/x86_64/)
  * sur le lien ci-dessus, choisissez n'importe quel miroir
  * puis l'image .iso en **MINIMAL**

## Setup VM

Seuls les éléments importants sont indiqués ici. Le reste on n'importe que peu (pour nous en tout cas), utilisez les choix par défaut.

* lancer Virtualbox
* créer une nouvelle VM
  * choisissez un nom arbitraire
  * 512Mo RAM
  * Disque VDI en 8Go, dynamiquement alloué
* une fois créée, modifier ses paramètres
  * menu "Affichage" : mettez la mémoire Vidéo à 128Mo (ça évitera des pb lors de l'installà
  * menu "Réseau" : mettez une interface NAT (et c'est tout)
* lancer la VM
  * au lancement de la VM, VirtualBox nous demande un disque de démarrage
  * donnez lui le .iso de CentOS7 précédemment téléchargé

## Installation CentOS7

* Premier écran d'install
  * choisir la langue "English"
  * valider
* Deuxième écran d'install
  * pour l'heure, sélectionner la zone "Europe/Paris"
  * pour le clavier
    * si vous êtes sous Windows/Linux : French
    * si vous êtes sous Mac : French Macintosh
  * partitionnement
    * cliquer dessus puis faire "Done" direct
    * ça permet de valider le partitionnement par défaut
  * réseau
    * activez la carte (le petit bouton ON/OFF en haut à droite)
  * valider
* Troisième écran d'install
  * définissez **un mot de passe SIMPLE** pour l'utilisateur `root`
  * définissez un utilisateur
    * nom simple
    * mot de passe simple
    * cocher la case "Faire de cet utilisateur un administrateur"
  * attendre la fin d'install
* Reboot
* S'assurer que la VM démarre

## Préparation du patron 

Une fois la VM allumée et démarrée :
* loggez-vous avec le user créé
* installer des paquets avec la commande `sudo yum install <PACKAGE>`
  * les paquets suivants seront utilisés pendant les cours de réseau :
    * `tcpdump`
    * `nc`
    * `bind-utils`
    * `nmap`
    * `traceroute`
* désactiver SELinux
  * `setenforce 0` pour le désactiver immédiatement, de façon temporaire
  * modification du fichier `/etc/selinux/config` pour le désactiver après reboot, de façon permanente
    * dans le fichier, remplacer `enforcing` par `permissive`

Et on est bons. 

Eteindre cette VM et ne jamais la rallumer, ne l'utiliser que pour créer des clones. 

