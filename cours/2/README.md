# Cours 2 - Notion de port et routage

- [Cours 2 - Notion de port et routage](#cours-2---notion-de-port-et-routage)
- [I. Notion de ports](#i-notion-de-ports)
  - [1. Concept](#1-concept)
  - [2. En pratique](#2-en-pratique)
- [I. Routage](#i-routage)
  - [1. Concept du routage](#1-concept-du-routage)
  - [2. Cas concret : routage entre deux LANs](#2-cas-concret--routage-entre-deux-lans)
  - [3. NAT](#3-nat)
    - [A. IP privées/IP publiques](#a-ip-priv%c3%a9esip-publiques)
    - [B. Why NAT ?](#b-why-nat)
    - [C. NAT à la rescousse](#c-nat-%c3%a0-la-rescousse)

# I. Notion de ports

## 1. Concept

Afin d'établir une connexion entre deux hôtes, les deux hôtes ont besoin d'ouvrir un *port*.  
Un *port* est un point d'entrée sur une interface réseau donnée. Il existe 65535 ports sur chaque interface.  

> On peut s'imaginer un PC comme une maison, et les *ports* les différentes portes qui permettent de rentrer dans ladite maison.

Il existe deux types de port : TCP et UDP. Nous nous cantonnerons à une explication simple pour le moment : 
* TCP : permet une connexion stable mais lente
  * utilisé par exemple pour transporter le HTTP, ou les messages dans des discussions instantanées
* UDP : permet une connexion instable mais rapide
  * utilisé par exemple dans les jeux en ligne

## 2. En pratique

En pratique, **on définit comme *serveur* une machine qui *écoute* sur un port**. C'est à dire que cette machine a placé une application derrière ce port, et l'application attend des connexions.  
Il est alors possible pour un client d'établir une connexion sur le port en question.

On parle de **relation client/serveur**.

On peut lister les ports en *écoute* avec les commandes dédiées : [`netstat` et `ss`](../memo/linux_commandes.md#netstat-ou-ss) : 

```
# Windows/MacOS : lister les connexion en écoute, en tcp
$ netstat -n -l -p tcp

# GNU/Linux : lister les connexions en écoute, en tcp
$ ss -n -l -t
```

---

Le client doit connaître à l'avance le port et l'adresse du serveur auquel il veut se connecter. C'est le cas lorsque vous vous connectez à un serveur web :
* vous connaissez son adresse (IP ou nom de domaine)
* vous le visitez sur le port 80 (HTTP) ou 443 (HTTPS)

> Les ports 80 pour HTTP et 443 pour HTTPS sont des conventions. Rien d'obligatoire. Mais utile pour être reconnu comme un site "normal".

Dernière chose : pour que la connexion entre le client et le serveur s'établisse, **le client doit lui aussi ouvrir un port**. Une fois que le client a ouvert un port, il peut se connecter au port du serveur. 

> Le port ouvert par le client est un numéro aléatoire.

On peut lister les ports ouverts en tant que client avec les commandes dédiées : [`netstat` et `ss`](../memo/linux_commandes.md#netstat-ou-ss) : 

```
# Windows/MacOS : lister les connexions client en TCP et les ports en écoute
$ netstat -n -p tcp

# GNU/Linux : lister les connexions client en TCP
$ ss -n -t
```

# I. Routage

## 1. Concept du routage

**Le routage permet à deux réseaux IP de communiquer.** L'équipement qui effectue du routage est appelé un ***routeur***.

Un *routeur* a pour rôle d'acheminer les paquets d'un réseau à un autre. Il fait le pont entre les deux réseaux. 

S'il permet aux clients d'un réseau local (LAN) de sortir vers d'autres réseaux, on l'appelle la **passerelle** de ce réseau.

---

Chaque équipement (PC, routeurs, serveurs, etc.) possède une ***table de routage***. Cette *table de routage* indique à la machine les réseaux qu'elle peut joindre.

Dans une table de routage, chaque ligne est appelée une ***route***. Chaque *route* contient :
* l'adresse du réseau à joindre
* comment s'y rendre
  * soit on y est directement connecté
  * soit il faut passer par une passerelle

Il existe une route particulière appelée ***route par défaut***. Elle indique le chemin à prendre pour n'importe quel réseau qui n'est pas explicitement précisé dans les autres *routes* de la *table de routage*. C'est un panneau "toutes directions" quoi, clairement.

> La *route par défaut* est notée comme une *route* vers le réseau `default`, ou vers le réseau `0.0.0.0.`

---

On peut afficher la table de routage d'un PC/serveur avec :
```
# Windows, MacOS et GNU/Linux
$ netstat -nr       # option -r pour "routing table"

# Windows only 
$ route print
$ route print -4    # que l'iIPv4

# GNU/Linux only
$ ip route show
$ ip r s            # same, juste plus court
```

## 2. Cas concret : routage entre deux LANs

Supposons deux réseaux locaux (LAN) :

```
    LAN1 : 192.168.1.0/24         LAN2 : 192.168.2.0/24
+---------------------------+ +---------------------------+
|                           | |                           |
|                           | |                           |
|                           | |                           |
|                           | |                 +---+     |
|                     .254+-+-+-+               |PC2|     |
|                         |  R  |               +---+     |
|                         |     |                 .57     |
|                         +-+-+-+.254                     |
|       .13                 | |                           |
|     +---+                 | |                           |
|     |PC1|                 | |                           |
|     +---+                 | |                           |
|                           | |                           |
|                           | |                           |
|                           | |                           |
+---------------------------+ +---------------------------+
```
Réseaux :

| Nom du réseau | Adresse du réseau |
| ------------- | ----------------- |
| LAN1          | `192.168.1.0/24`    |
| LAN2          | `192.168.2.0/24`    |

Machines : 

| Nom     | Adresse LAN1  | Adresse LAN2  |
| ------- | ------------- | ------------- |
| Routeur | `192.168.1.254` | `192.168.2.254` |
| PC1 | `192.168.1.13` | x |
| PC2 | x | `192.168.2.57` |

On dit ici que : 
* le routeur est la passerelle de PC1 et PC2
* le routeur est la passerelle du LAN1 et du LAN2
* `192.168.1.254/24` est la passerelle du LAN1 et donc, du PC1
* `192.168.2.254/24` est la passerelle du LAN2 et donc, du PC2

PC1 pourra parler à PC2 et réciproquement, en désignant le routeur comme leur passerelle :
* pour PC1, `192.168.1.254/24` est la passerelle vers `192.168.2.0/24`
* pour PC2, `192.168.2.254/24` est la passerelle vers `192.168.1.0/24`

## 3. NAT

Le ***NAT*** est un cas particulier du routage. Le *NAT* permet à un routeur de router des paquets qui passent d'un domaine privé (adresse locale dans un *LAN*) à un domaine public (un réseau étendu ou *WAN*, comme Internet).

### A. IP privées/IP publiques

Avant de rentrer dans le détail du NAT, il est nécessaire de distinguer les adresses IP publiques des adresses privées.

Une IP est dite *privée* si elle appartient à l'une des plages suivantes : 
* `192.168.0.0/16`
  * par exemple, le réseau `192.168.34.0/24` est privé
* `10.0.0.0/8`
  * par exemple, le réseau `10.33.0.0/22` est privé
* `172.16.0.0/12`
  * par exemple, le réseau `172.16.37.0/24` est privé

Cette convention fait partie du protocole IP lui-même.  
**Ces IPs "privées" peuvent être utilisées par n'importe qui de façon arbitraire pour attribuer des IPs à des machines dans des réseaux locaux (ou *LAN*).**

> Il existe d'autres plages IPs que nous allons ignorer pour le cours, pour simplifier le propos. Vous pourrez trouver [une liste exhaustive des plages d'adresses IP réservées sur Wikipedia par exemple](https://en.wikipedia.org/wiki/Reserved_IP_addresses).

---

Les IPs n'appartenant à aucune plage réservée sont dites "publiques". Les IPs publiques permettent d'avoir une adresse sur Internet, un réseau global, qui permet de relier tous les autres réseaux entre eux.  

Ce sont des organismes comme le RIPE qui gère l'attribution des adresses IP publiques. [Toutes les infos ici](https://www.iana.org) pour les curieux.

**Il est impossible, en utilisant une IP publique, de joindre une IP privée.**

### B. Why NAT ?

Lorsqu'un paquet transite sur le réseau, il contient l'adresse IP de la machine de destination et celle de la machine source. 

Dans le cas très courant où le client d'un réseau local veut joindre une machine sur Internet, le routage simple ne suffit plus.

Prenons le cas suivant :
```
    LAN1 : 192.168.1.0/24             WAN (internet)
+---------------------------+ +---------------------------+
|                           | |                           |
|                           | |                           |
|                           | |          98.24.14.144     |
|                           | |     C           +---+     |
|            D        .254+-+-+-+<--------------+ S |     |
|      +----------------->+  R  |               +-+-+     |
|      |                  |     |                 ^       |
|      |                  +-+---+-----------------+       |
|      |.13               | | |87.238.12.14   B           |
|     +---+               | | |                           |
|     |PC1+<--------------+ | |                           |
|     +---+        A        | |                           |
|                           | |                           |
|                           | |                           |
|                           | |                           |
+---------------------------+ +---------------------------+
```

Les lettres `A`, `B`, `C` et `D` représente les différents moments dans la vie du paquet :
* `A` : aller du paquet : du PC1 au routeur
* `B` : aller du paquet : du routeur au serveur
* `C` : retour du paquet : du serveur au routeur
* `D` : retour du paquet : du routeur au PC1

Réseaux :

| Nom du réseau | Adresse du réseau  |
| ------------- | ------------------ |
| LAN1          | `192.168.1.0/24`   |
| Internet      | Adresses publiques |

Machines : 

| Nom     | Adresse LAN1    | IP publique    |
| ------- | --------------- | -------------- |
| Routeur | `192.168.1.254` | `87.238.12.14` |
| PC1     | `192.168.1.13`  | x              |
| Serveur | x               | `98.24.14.144` |

---

Examinons le paquet qui transite sur le réseau, à chaque étape de son transit : 

| Moment | Adresse source | Adresse destination |
| ------ | -------------- | ------------------- |
| `A`    | `192.168.1.13` | `98.24.14.144`      |
| `B`    | `192.168.1.13` | `98.24.14.144`      |
| `C`    | `98.24.14.144` | `192.168.1.13`      |
| `D`    | `98.24.14.144` | `192.168.1.13`      |

Si on examine bien le tableau, à partir de `C`, le serveur renvoie le paquet. Il souhaite renvoyer le paquet vers `192.168.1.13`. **Or, c'est une adresse privée**, il ne peut donc pas joindre cette IP en utilisant son IP publique.

**En réalité, le paquet ne repartira jamais du serveur, et PC1 ne recevra jamais sa réponse. Les étapes `C` et `D` n'existent pas.**

### C. NAT à la rescousse

Le *NAT* permet de résoudre ce problème. Le concept du *NAT* est de changer les adresses dans le paquet, à la volée. 

Le *NAT* pour *Network Address Translation* est une fonctionnalité des routeurs. En utilisant le *NAT* sur un routeur, on lui informe qu'il doit changer les adresses IP des paquets qui le traversent.

En reprenant l'exemple du dessus, mais en demandant au routeur de faire du *NAT*, le tableau des différentes étapes de vie serait le suivant : 
| Moment | Adresse source | Adresse destination |
| ------ | -------------- | ------------------- |
| `A`    | `192.168.1.13` | `98.24.14.144`      |
| `B`    | `87.238.12.14` | `98.24.14.144`      |
| `C`    | `98.24.14.144` | `87.238.12.14`      |
| `D`    | `98.24.14.144` | `192.168.1.13`      |

**Le routeur change l'IP source du paquet qui sort.** Ca se passe avant l'étape `B`.   
**Puis change de nouveau l'IP de destination du paquet de réponse.** Ca se passe après l'étape `C`.

Ceci a pour effet : 
* le serveur peut correctement répondre à l'IP publique (étape `C`)
* le client ne voit aucune différence : le paquet qui lui revient a une adresse source et une adresse destination qui sont cohérentes avec ce qu'il a envoyé (voir les étapes `A` et `D`)