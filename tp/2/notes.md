# 2019 B1 Réseau TP2

| Etudiant(s)    | Créa VM | `nmap`/`ss` | Get SSHD | Set SSHD | `nc` 1 | `nc` 2 | Wireshark | Routage | `traceroute` | Hostnames | Forme | Note |
| -------------- | ------- | ----------- | -------- | -------- | ------ | ------ | --------- | ------- | ------------ | --------- | ----- | ---- |
| abdoul-wahab   | x       | x           | x        | -        | x      | -      | o         | x       | x            | x         | x     | 16   |
| Auriol         | x       | x           | x        | x        | x      | x      | x         | x       | o            | x         | x     | 17   |
| BergezCasalou  | x       | x           | -        | -        | x      | x      | x         | x       | o            | x         | -     | 16   |
| Boileau        | x       | x           | x        | -        | x      | o      | o         | x       | x            | x         | x     | 14   |
| bonnin         | x       | x           | x        | x        | x      | -      | x         | x       | o            | x         | o     | 15   |
| BORDREZ        | x       | -           | x        | -        | x      | x      | o         | o       | o            | x         | -     | 14   |
| Boudeau        | x       | -           | -        | -        | x      | x      | o         | o       | o            | x         | o     | 12   |
| buton          | x       | o           | o        | -        | o      | o      | -         | -       | o            | o         | -     | 6    |
| Chancerel      | x       | x           | x        | x        | x      | o      | x         | -       | o            | o         | o     | 13   |
| Christophe     | x       | -           | x        | x        | -      | o      | o         | -       | o            | o         | +     | 10   |
| Crenn          | x       | x           | -        | x        | x      | +      | x         | x       | x            | x         | x     | 18   |
| dayot          | x       | x           | x        | x        | x      | x      | x         | x       | x            | x         | x     | 18   |
| DEMONTOUX      | x       | x           | x        | +        | x      | x      | x         | x       | x            | x         | x     | 19,5 |
| Dournet        | x       | o           | -        | -        | -      | o      | -         | x       | x            | x         | x     | 12   |
| Dugoua         | x       | x           | x        | x        | x      | x      | x         | x       | x            | x         | -     | 17   |
| Dupuis         | x       | -           | x        | x        | -      | -      | -         | x       | -            | x         | -10   | 13   |
| Duvigneau      | x       | +           | x        | x        | x      | x      | x         | x       | x            | x         | -     | 17   |
| Ezzahi         | x       | x           | x        | -        | -      | o      | o         | o       | o            | o         | o     | 9    |
| Ferreira Silva | x       | -           | x        | o        | x      | x      | -         | x       | x            | x         | x     | 16   |
| Ford           | x       | -           | x        | -        | x      | o      | o         | o       | o            | x         | x     | 10   |
| Garcia         | x       | x           | x        | x        | x      | x      | -         | x       | -            | x         | -     | 14   |
| GHOUTHI        | x       | -           | x        | x        | x      | o      | o         | o       | o            | o         | x     | 7    |
| JUDEAUX        | x       | x           | x        | o        | x      | o      | o         | o       | o            | o         | o     | 6    |
| Labedade       | x       | x           | x        | -        | x      | o      | -         | x       | x            | x         | -     | 14   |
| Lachamp        | x       | x           | x        | x        | o      | o      | o         | o       | o            | x         | x     | 7    |
| LAFOREST       | x       | -           | x        | o        | x      | o      | o         | o       | o            | o         | -     | 7    |
| LAUTIER        | x       | -           | x        | -        | -      | o      | o         | o       | o            | o         | x     | 6    |
| LEFEBVRE       | x       | x           | x        | x        | x      | x      | -         | x       | x            | x         | x     | 18   |
| MARECHAL       | x       | x           | x        | x        | x      | x      | x         | o       | o            | o         | o     | 14   |
| Mégy           | x       | x           | x        | x        | x      | x      | x         | o       | o            | o         | o     | 13   |
| michon         | x       | -           | x        | -        | x      | o      | -         | x       | x            | x         | o     | 13   |
| mickhail       | x       | x           | x        | x        | x      | x      | x         | x       | o            | x         | x     | 16   |
| MONNET         | x       | x           | x        | x        | x      | x      | ++        | x       | x            | x         | x     | 19,5 |
| Ranaivoson     | x       | -           | x        | -        | x      | o      | o         | -       | o            | o         | x     | 13   |
| ROUZIERE       | x       | x           | x        | -        | x      | x      | x         | x       | x            | x         | -     | 18   |
| Tawk           | x       | x           | x        | x        | x      | x      | +         | x       | o            | x         | o     | 17   |
| Texier         | x       | -           | x        | -        | x      | o      | o         | o       | -            | x         | x     | 13   |
| Vella          | x       | x           | x        | x        | x      | o      | o         | o       | o            | o         | x     | 10   |
| VERRIER        | x       | x           | x        | x        | x      | x      | +         | x       | x            | x         | -     | 18   |
| VILALLONGA     | x       | x           | x        | x        | x      | x      | x         | x       | x            | x         | x     | 18,5 |
| VOISIN         | x       | x           | x        | -        | x      | x      | x         | x       | x            | x         | o     | 16   |
| Zajac          | x       | o           | -        | o        | x      | o      | x         | -       | o            | o         | o     | 11   |
| ZOGHBI         | x       | -           | x        | x        | x      | x      | +         | x       | x            | x         | o     | 17   |
