# B1 Réseau 2019 - TP2

# TP 2 - Machine virtuelle, réseau, serveurs, routage simple 

Pour ce TP (et probablement pour beaucoup de TPs à l'avenir), on va se servir de machines virtuelles.  

L'OS choisi est CentOS7.
* c'est un OS GNU/Linux
  * beaucoup plus adapté d'un point de vue éducatif (que MacOS ou Windows) : on a la main complète sur le système
  * il n'y pas de boîte noire quant au fonctionnement du réseau par exemple
* CentOS
  * distribution orientée serveur
  * réputée pour sa robustesse, sa durée de vie et sa sécurité 

---

Le but du TP est d'appréhender un peu la gestion du réseau sous une machine GNU/Linux. Revoir un peu quelques notions vues au premier TP (IP, MAC, ports, firewall, etc) et les approfondir.

On abordera aussi les notions de clients/serveurs avec un serveur SSH et un serveur Web.

# Déroulement et rendu du TP 
* vous utiliserez un l'hyperviseur de votre choix parmi : 
  * [Virtualbox](https://www.virtualbox.org/wiki/Downloads) (recommandé)
  * VMWare Workstation
  * j'utiliserai VirtualBox pour ma part, c'est avec lui que les exemples seront donnés
* les machines virtuelles : 
  * l'OS **devra être** [**CentOS 7 en version minimale**](http://ftp.rezopole.net/centos/7.7.1908/isos/x86_64/)
  * pas d'interface graphique (que de la ligne de commande)
* **désactiver le firewall de votre PC pour les TPs**. svpliz.
  
---

* il y a beaucoup de ligne de commande dans ce TP, préférez les copier/coller aux screens
  * vous pourrez copiez/coller simplement à partir du [II.2.](#2-ssh)
  * soit vous screenez ce qu'il y a avant, soit vous prenez des notes et vous refaites une fois que vous pouvez copier/coller (c'est très vite fait à refaire)
* le rendu doit toujours se faire [au même format](../README.md)

# Hints généraux
* **pour vos recherches Google** (ou autres) : 
  * **en anglais**
  * **précisez l'OS et la version** dans vos recherches ("centos 7" ici)
* dans le TP, **lisez en entier une partie avant de commencer à la réaliser.** Ca donne du sens et aide à la compréhension
* **allez à votre rythme.** Le but n'est pas de finir le TP, mais plutôt de bien saisir et correctement appréhender les différentes notions
* **n'hésitez pas à me demander de l'aide régulièrement** mais essayez toujours de chercher un peu par vous-mêmes avant :)

# Sommaire

- [B1 Réseau 2019 - TP2](#b1-r%c3%a9seau-2019---tp2)
- [TP 2 - Machine virtuelle, réseau, serveurs, routage simple](#tp-2---machine-virtuelle-r%c3%a9seau-serveurs-routage-simple)
- [Déroulement et rendu du TP](#d%c3%a9roulement-et-rendu-du-tp)
- [Hints généraux](#hints-g%c3%a9n%c3%a9raux)
- [Sommaire](#sommaire)
- [I. Création et utilisation simples d'une VM CentOS](#i-cr%c3%a9ation-et-utilisation-simples-dune-vm-centos)
  - [1. Création](#1-cr%c3%a9ation)
  - [2. Installation de l'OS](#2-installation-de-los)
  - [3. Premier boot](#3-premier-boot)
  - [4. Configuration réseau d'une machine CentOS](#4-configuration-r%c3%a9seau-dune-machine-centos)
  - [5. Appréhension de quelques commandes](#5-appr%c3%a9hension-de-quelques-commandes)
- [II. Notion de ports](#ii-notion-de-ports)
  - [1. SSH](#1-ssh)
  - [2. Firewall](#2-firewall)
    - [A. SSH](#a-ssh)
    - [B. Netcat](#b-netcat)
  - [3. Wireshark](#3-wireshark)
- [III. Routage statique](#iii-routage-statique)
  - [0. Rappels et objectifs](#0-rappels-et-objectifs)
  - [1. Préparation des hôtes (vos PCs)](#1-pr%c3%a9paration-des-h%c3%b4tes-vos-pcs)
    - [Préparation avec câble](#pr%c3%a9paration-avec-c%c3%a2ble)
    - [Préparation VirtualBox](#pr%c3%a9paration-virtualbox)
    - [Check](#check)
    - [Activation du routage sur les PCs](#activation-du-routage-sur-les-pcs)
      - [Windows](#windows)
      - [GNU/Linux](#gnulinux)
      - [MacOS](#macos)
  - [2. Configuration du routage](#2-configuration-du-routage)
    - [A. PC1](#a-pc1)
    - [B. PC2](#b-pc2)
    - [C. VM1](#c-vm1)
    - [D. VM2](#d-vm2)
    - [E. El gran final](#e-el-gran-final)
  - [3. Configuration des noms de domaine](#3-configuration-des-noms-de-domaine)
- [Bilan](#bilan)
- [Annexe 1 : Désactiver SELinux](#annexe-1--d%c3%a9sactiver-selinux)
  
---
# I. Création et utilisation simples d'une VM CentOS

Dans cette partie, on va créer une machine virtuelle :
* création de la VM dans l'hyperviseur
* installation de l'OS (CentOS7)
* configuration élémentaire de la machine
  * on se concentrera surtout sur la partie réseau obviously

## 1. Création

Créer une machine virtuelle en utilisant un [`iso` de CentOS 7 **version minimale**](http://ftp.rezopole.net/centos/7.7.1908/isos/x86_64/).  
Avant la création, la seule chose qu'on va toucher, c'est le réseau. Assurez-vous que : 
* votre VM possède une interface de type NAT
* votre VM possède une interface de type *réseau privé hôte* ou *host-only network*
  * ce réseau *host-only* **ne doit pas** avoir de serveur DHCP activé

## 2. Installation de l'OS

CentOS 7 est fourni avec un installateur graphique (on peut cliquer et faire des bails et tout), afin de faciliter l'installation de l'OS.  
On laisse la plupart des éléments par défaut excepté : 
* activation de la carte réseau NAT
* définition d'un utilisateur administrateur ainsi que de son mot de passe
* définition du mot de passe de l'utilisateur `root`
* timezone correcte (fuseau horaire)
* disposition du clavier en *fr*
* OS en anglais (svpliz)

## 3. Premier boot

Que de la ligne de commande, alors petit rappel :
* `cd` change de dossier
* `ls` liste les fichiers/dossiers du dossier actuel
* `pwd` affiche le répertoire où on est actuellement
Avec ces 3 là vous pouvez vous baladez un peu partout. Sachez juste que la *racine* du disque dur n'est pas à `C:` mais simplement à `/`. Donc `cd /` vous amène dans un dossier qui contient tous les autres.   

En plus on va avoir besoin de : 
* `man` : affiche le manuel d'une commande. Avec celle-ci vous pouvez conquérir toutes les autres :)
* `ifconfig` (obsolète) ou `ip a` (à jour) pour avoir des infos sur vos cartes réseau
* `cat` qui permet de lire le contenu d'un fichier (un fichier texte par exemple)
  * par exemple `cat /etc/os-release` permet d'ouvrir et afficher le contenu du fichier texte `os-release`, se trouvant dans le dossier `/etc`
* `nano` (simple, mais nul) ou `vi`/`vim` (complexe, mais puissant) pour éditer des fichiers. Un bloc-note/notepad quoi.

> **N'oubliez pas de [désactiver SELinux](#annexe-1--désactiver-selinux)** pour les TPs sur CentOS7.

## 4. Configuration réseau d'une machine CentOS

🌞 Déterminer la liste de vos cartes réseau, les informations qui y sont liées, et la fonction de chacune.
* allez faites moi un p'tit tableau markdown, du style :

Name | IP | MAC | Fonction
--- | --- | --- | ---
`enp0s4` | `10.1.1.1` | `de:ad:be:ef` | Carte NAT (accès internet)

---

🌞 [Changer la configuration de la carte réseau Host-Only](../../cours/memo/linux_centos_network.md#définir-une-ip-statique)
* changer vers une autre IP statique
* prouver que l'IP a bien changé
* `ping` du PC hôte, depuis la VM, en utilisant les IP du réseau Host-Only

## 5. Appréhension de quelques commandes

Après avoir joué avec `ip a` et `ping`, on va creuser un peu plus loin.

Pour savoir comment utiliser une commande, vous pouvez utiliser `-h` ou le `man`.  

> Si vous n'avez pas une de ces commandes il faudra l'installer avec `yum`.

---

`nmap`
* outil de scan de réseau
* capable d'une multitude de scan, scan TCP, ICMP, etc (on y reviendraaaa)

🌞 faites un scan `nmap` du réseau host-only
* déterminer l'adresse IP des machines trouvées
* vous devriez trouver votre PC hôte (au moins). Vérifiez avec une commande sur votre PC hôte que c'est la même MAC que dans le scan `nmap`

---

`nc`
* permet d'effectuer des connexions à des ports TCP ou UDP
* déjà utilisé lors du précédent TP, on y revient plus tard dans celui-ci :)

---

`ss`
* permet de lister les connexions actives actuellement sur la machine
* par exemple, les connexions TCP, UDP

```
# Lister les connexions actives en TCP (-t), UDP (-u)
$ ss -t -u

# Liste les ports en écoute (-l), en TCP (-t) et UDP (-u)
$ ss -l -t -u

# Liste les ports en écoute, en TCP et UDP, sans traduire les numéros de protocole (-n)
$ ss -l -t -u -n

# Idem, mais affiche en plus le processus (le "programme", ou "logiciel", ou "application") qui est en jeu dans cette connexion (-p)
$ ss -l -t -u -n -p
$ ss -ltunp # plus rapide
```

🌞 Utiliser `ss` pour lister les ports TCP et UDP en écoute sur la machine
* déterminer quel programme écoute sur chacun de ces ports

---

Un p'tit dernier. `tcpdump`
* c'est Wireshark. Mais en ligne de commande. Yerk.
* on peut aussi capturer avec `tcpdump` puis exporter vers un Wireshark graphique :)
* rien à faire pour le moment, on y reviendra plus tard
* essayez quand même de le lancer vite fait, pendant un `ping` vers votre PC hôte par exemple

```
# Dans un premier terminal
$ ping <HOST>
$ ping 192.168.56.1

# Dans un deuxième terminal
$ sudo tcpdump -i <INTERFACE_NAME>
$ sudo tcpdump -i enp0s8
```

# II. Notion de ports

## 1. SSH

> Pour rappel, il faut [désactiver SELinux](#annexe-1-désactiver-selinux).

**SSH** est un protocole pour se **connecter sur un serveur à distance** :
* on installe un serveur SSH sur une machine
* on configure le serveur SSH pour écouteur sur une adresse IP et un port spécifiques
  * par convention, on met souvent le serveur SSH sur le port 22 
* on lance le serveur SSH
* quelqu'un qui est peut joindre la machine peut alors utiliser un **client SSH** pour se connecter au serveur

On l'utilise entre autres pour des connexions à distance sur un ordinateur, lorsqu'un accès physique est compliqué.

**Mais** on va aussi s'en servir pour se connecter à la VM, entre autres parce que : 
* permet de gérer toutes les VMs directement depuis votre hôte
* permet de faire des copier/coller izi, plutôt que de galérer avec la console

---

CentOS 7 est équipé d'un serveur SSH par défaut.

🌞 Déterminer sur quelle(s) IP(s) et sur quel(s) port(s) le serveur SSH écoute actuellement.

🌞 Effectuer une connexion SSH
* depuis votre PC hôte, vers la VM
* utiliser `netstat` ou `ss` sur l'hôte et mettre en évidence la connexion SSH active

## 2. Firewall

CentOS 7 est aussi équipé d'un pare-feu. Par défaut, il bloque tout, à part quelques services comme `ssh` justement.  

> 📚  RDV dans la section dédiée du mémo Linux pour [les commandes permettant de manipuler le firewall de CentOS](../../cours/memo/linux_centos_network.md#interagir-avec-le-firewall).

### A. SSH

🌞 Modifier le port du service SSH
* il faut modifier le fichier `/etc/ssh/sshd_config`
  * trouver la ligne qui indique le port sur lequel écoute le serveur SSH
  * modifier la ligne pour écouter sur un autre port de votre choix
    * valeur entre 1024 et 65535 :)

> Si une ligne commence par un `#` c'est un commentaire. Il faut enlever le `#` pour que la ligne soit effective.

Puis relancer le service ssh
* `sudo systemctl restart sshd`

🌞 Vérifier que le service SSH écoute sur le nouveau port choisi
* avec une commande dans la VM
* puis tester la connexion (il faudra préciser le port côté client quand vous testez la connexion)
* s'assurer qu'elle échoue : Le firewall la bloque.

🌞 [Ouvrir le port correspondant du firewall](../../cours/memo/linux_centos_network.md#interagir-avec-le-firewall)
* puis vérifier la bonne connexion

### B. Netcat

🌞 Comme dans le précédent TP, on va faire un ptit chat. La VM sera le serveur, le PC sera le client.
* dans un premier terminal sur la VM
  * lancer un serveur `netcat` dans un terminal (commande `nc -l`)
  * le serveur doit écouter sur le port de votre choix (toujours avec 1024 < PORT < 65535), en TCP
  * il faudra autoriser ce port dans le firewall
* dans un deuxième terminal sur l'hôte
  * se connecter au serveur `netcat` (commande `nc`)
* dans un troisième terminal sur la VM
  * utiliser `ss` pour visualiser la connexion `netcat` en cours

🌞 Inversez les rôles, le PC est le serveur netcat, la VM est le client.

## 3. Wireshark

Utiliser Wireshark pendant un `netcat`
* Pendant que le PC joue le rôle du serveur
* Puis pendant que la VM joue le rôle du serveur

🌞 Essayez de mettre en évidence
* le fait que le contenu des messages envoyés avec `netcat` est visible dans Wireshark
* les trois messages d'établissement de la connexion
  * peu importe qui est client et qui est serveur une fois la connexion établie
  * mais au moment de l'établissement de la connexion, il y a trois messages particuliers qui sont échangés
  * ces trois messages ont pour source le client
  * n'hésitez pas à m'appeler en cas de doute

# III. Routage statique

> Pour rappel, il faut [désactiver SELinux](#annexe-1--désactiver-selinux).  

⚠️ ⚠️ ⚠️ **Pour cette partie, désactivez aussi votre interface NAT sur vos VMs.** ⚠️ ⚠️ ⚠️  
Vous ne les activerez que lorsque strictement nécessaire (quand il faut aller sur internet, pour installer des paquets par exemple). **Désactivez les dans VirtualBox directement.**

---

Le **routage**, c'est le fait d'utiliser une machine comme pivot, comme passerelle (un routeur), entre deux réseaux, afin qu'il fasse passer le trafic d'un réseau à un autre.  
Le routage statique consiste à définir de façon simple les routes utilisables par le routeur et les machines, sur chacune d'entre elles. C'est l'administrateur qui les définit à la main.  

Une *route* c'est le chemin à prendre pour joindre un réseau donné. Plutôt que d'indiquer une route physique comme un panneau de signalisation, une *route* en réseau pointe vers une *passerelle*. Une *passerelle* est une machine qui permet de changer de réseau.

## 0. Rappels et objectifs

> Pour cette partie, mettez vous par 2, comme au TP1. 

**La dernière fois, dans le TP 1**, on a fait ça :
* deux PCs reliés avec un câble
* les interfaces Ethernet des deux PCs étaient dans le même réseau pour pouvoir communiquer
* "être dans le même réseau" c'est avoir une adresse IP dans le même réseau IP
```
  Internet           Internet
     |                   |
    WiFi                WiFi
     |                   |
    PC 1 ---Ethernet--- PC 2
```

---

Et dans ce TP 2 on a fait ça : 
* deux PCs reliés avec un câble
  * l'un est physique, c'est votre hôte
  * l'autre est virtuel, c'est la VM
  * le câble, c'est le réseau host-only
* les interfaces host-only des deux PCs (hôte et VMs) sont dans le même réseau pour pouvoir communiquer
* **en fait c'est la même chose que le TP1, clairement.**
  * on a juste ajouté une couche "virtuelle"
    * le câble Ethernet a été remplacé par un réseau host-only
    * le PC2 a été remplacé par une VM

```
   PC 1
    |
Host-only 1
    |
   VM 1
```

---

Objectif, encore un schéma moche :
```
 Internet            Internet
    |                    |
  WiFi                  WiFi
    |                    |
   PC 1  ---Ethernet--- PC 2
    |                    |
Host-only 1          Host-only 2
    |                    |
   VM 1                 VM 2
```

---

Vos PCs vont devenir de vrais routeurs ; on pourrait les appeler R1 et R2, plutôt que PC1 et PC2 !

Dans la suite on appelle :
* réseau `1` le réseau host-only 1
* réseau `2` le réseau host-only 2
* réseau `12` le réseau formé par le câble Ethernet (parce qu'il lie le `1` et le `2`)

---

**Tableau d'adressage** :

Name | Réseau `1` | Réseau `2` | Réseau `12`
--- | --- | --- | ---
PC1 | `10.2.1.1/24` | X | `10.2.12.1/30`
PC2 | X | `10.2.2.1/24` | `10.2.12.2/30`
VM1 | `10.2.1.2/24` | X | X
VM2 | X | `10.2.2.2/24` | X

> Pour vous en souvenir facilement des IPs que je demande : `10.numeroTP.nomréseau.numéromachine`

## 1. Préparation des hôtes (vos PCs)

> **Pour rappel, dans cette partie, désactivez votre interface NAT sur vos VMs.** Désactivez les dans VirtualBox directement.

### Préparation avec câble
Faites en sorte que :
* vos deux PCs puissent se ping à travers le câble
* vos cartes Ethernet doivent être dans le réseau `12` : `10.2.12.0/30`

### Préparation VirtualBox
Modifier vos réseaux host-only pour qu'ils soient :
* PC1 : réseau `1` : `10.2.1.0/24`
* PC2 : réseau `2` : `10.2.2.0/24`

Créez (ou réutilisez) une VM, et modifiez son adresse :
* VM1 (sur PC1) : `10.2.1.2/24`
* VM2 (sur PC2) : `10.2.2.2/24`

### Check
Assurez vous que :
* PC1 et PC2 se ping en utilisant le réseau `12`
* VM1 et PC1 se ping en utilisant le réseau `1`
* VM2 et PC2 se ping en utilisant le réseau `2`

### Activation du routage sur les PCs

Pour que vos hôtes (vos PCs) puissent agir comme des routeurs, il va falloir faire un peu de configuration, qui va dépendre de votre OS.  

> Google it aussi, j'ai pas retesté sur Windows/MacOS cette année.

#### Windows
* modifier la clé registre dans le dossier registre `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\ Services\Tcpip\Parameters\` 
  * **cliquer sur ce dossier registre**
  * changer la variable `IPEnableRouter` en passant sa valeur de `0` à `1`
* vous pouvez reboot là (ça évite des pbs)
* activer le service de routage ("lancer l'application de routage")
  * `Win + R`
  * `services.msc` > `Entrée`
  * Naviguer à "Routage et accès distant"
    * Clic-droit > Propriétés
    * Pour le démarrage, sélectionner "Automatique"
    * `Appliquer`
    * `Démarrer`

#### GNU/Linux
* `sudo sysctl -w net.ipv4.conf.all.forwarding=1`

#### MacOS
* `sudo sysctl -w net.inet.ip.forwarding=1`


## 2. Configuration du routage

Le but va être de faire en sorte que VM1 puisse ping VM2 et réciproquement. Pour ce faire, avec du routage statique simple, on va devoir faire en sorte que tout le monde connaisse un chemin pour aller dans chacun des réseaux !  

Name | Réseau `1` | Réseau `2` | Réseau `12`
--- | --- | --- | ---
PC1 | `10.2.1.1/24` | connais pas | `10.2.12.1/30`
PC2 | connais pas | `10.2.2.1/24` | `10.2.12.2/30`
VM1 | `10.2.1.2/24` | connais pas | connais pas
VM2 | connais pas | `10.2.2.2/24` | connais pas

On va remplacer la plupart des "connais pas" par "je sais comment y aller" !

---

### A. PC1

PC1 accède déjà aux réseaux `1` et `12`, il faut juste lui dire comment accéder au réseau `2`
* Windows
  * `route add <IP_NET_2> mask 255.255.255.0 <IP_12_PC2>`
  * soit `route add 10.2.2.0/24 mask 255.255.255.0 10.2.12.2`
* GNU/Linux
  * **sur CentOS 7, voir [la page dédiée dans la page de procédures](../../cours/memo/linux_centos_network.md#ajouter-une-route-statique)**
  * `ip route add <IP_NET_2> via <IP_12_PC2> dev <INTERFACE_12_NAME>`
  * par exemple `ip route add 10.2.2.0/24 via 10.2.12.2 dev eth0`
* MacOS
  * `route -n add -net <IP_NET_2> <IP_12_PC2>`
  * `route -n add -net 10.2.2.0/24 10.2.12.2`
* **la règle peut se traduire par :**
  * *"si tu veux aller dans le réseau 10.2.2.0/24, passe par la machine 10.2.12.2"*
  * cela nécessite de déjà connaître la machine `10.2.12.2`, ici, c'est PC2, et PC1 le connaît déjà, donc on est bons :)

* 🌞 PC1 devrait pouvoir ping `10.2.2.1` (l'adresse de PC2 dans `2`)

---

### B. PC2
Faire l'opération inverse.  

🌞 PC2 devrait pouvoir ping `10.2.1.1` (l'adresse de PC1 dans `1`).

---

On en est là :  

Name | Réseau `1` | Réseau `2` | Réseau `12`
--- | --- | --- | ---
PC1 | `10.2.1.1/24` | **connais !** c'est par PC2 qu'il faut passer | `10.2.12.1/30`
PC2 | **connais !** c'est par PC1 qu'il faut passer | `10.2.2.1/24` | `10.2.12.2/30`
VM1 | `10.2.1.2/24` | connais pas | connais pas
VM2 | connais pas | `10.2.2.2/24` | connais pas

---

**Appelez-moi pour que je vérifie tout ça !**  

---

### C. VM1

> **Pour rappel, dans cette partie, désactivez votre interface NAT sur vos VMs.** Désactivez les dans VirtualBox directement.

VM1 n'a accès qu'au réseau `1`. **Tristesse.**  

🌞  Il faut dire à VM1 qu'elle peut joindre le réseau `2` en utilisant le lien qui l'unit avec `PC1`.
* [vous référer au mémo pour ajouter des routes sous CentOS](../../cours/memo/linux_centos_network.md#ajouter-une-route-statique)
* VM1 devrait pouvoir `ping 10.2.2.1`, l'adresse de PC2 dans le réseau `2`.
* `traceroute` aussi svp :)

> Par exemple, pour indiquer à une machine X portant l'ip `10.1.1.10` sur l'interface `eth0` qu'elle peut joindre un réseau `192.168.56.0/24` en passant par une passerelle portant l'IP `10.1.1.254`, on utiliserait la commande `ip route add 192.168.56.0/24 via 10.1.1.1.254 dev eth0`.

Name | Réseau `1` | Réseau `2` | Réseau `12`
--- | --- | --- | ---
PC1 | `10.2.1.1/24` | connais ! c'est PC2 la passerelle | `10.2.12.1/30`
PC2 | connais ! c'est PC1 la passerelle | `10.2.2.1/24` | `10.2.12.2/30`
VM1 | `10.2.1.2/24` | **connais !** c'est PC1 la passerelle | connais pas
VM2 | connais pas | `10.2.2.2/24` | connais pas

---

### D. VM2
Faire comme sur VM1.

> **Pour rappel, dans cette partie, désactivez votre interface NAT sur vos VMs.** Désactivez les dans VirtualBox directement.

🌞 VM2 devrait pouvoir ping `10.2.1.1`, l'adresse de PC1 dans le réseau `1`.
* `traceroute` encore !

Name | Réseau `1` | Réseau `2` | Réseau `12`
--- | --- | --- | ---
PC1 | `10.2.1.1/24` | connais ! c'est PC2 la passerelle | `10.2.12.1/30`
PC2 | connais ! c'est PC1 la passerelle | `10.2.2.1/24` | `10.2.12.2/30`
VM1 | `10.2.1.2/24` | connais ! c'est PC1 la passerelle | connais pas
VM2 | **connais !** c'est PC2 la passerelle | `10.2.2.2/24` | connais pas

---

### E. El gran final

🌞 VM1 et VM2 se `ping`.

\o/

> Et sans qu'elles connaissent le réseau `12`. Si c'est pas beau ça.

---

## 3. Configuration des noms de domaine

Pour notre TP : 
* donner un nom de domaine aux machines virtuelles 
  * PAS aux hôtes physiques, pour éviter de pourrir votre vraie configuration
* [remplir le fichier `hosts` des machines virtuelles](../../cours/memo/linux_centos_network.md#editer-le-fichier-hosts) **ET** des PCs physiques
  * Linux et MacOS : `/etc/hosts`
  * Windows : `C:\Windows\System32\drivers\etc\hosts`

Un tableau récapitulatif, qui distingue les notions de noms de domaine, nom d'hôtes et FQDN :

Host | Hostname |  Domain  |     FQDN
---- | -------- | -------- | ------------
PC1  |   `pc1`  | `tp2.b1` | `pc1.tp2.b1`
PC2  |   `pc2`  | `tp2.b1` | `pc2.tp2.b1`
VM1  |   `vm1`  | `tp2.b1` | `vm1.tp2.b1`
VM2  |   `vm2`  | `tp2.b1` | `vm2.tp2.b1`

🌞 utilisation des noms de domaines
* `vm1.tp2.b1` ping `vm2.tp2.b1`
* `vm1.tp2.b1` traceroute vers `vm2.tp2.b1`
* `vm1.tp2.b1` netcat serveur, `vm2.tp2.b1` netcat client

---

# Bilan
* **création de machine virtuelle** simple
* **configuration des cartes réseau** de la VM
* configuration d'**IP statique** sous vos OS natifs et CentOS7
* **gestion de réseau élémentaire sous Linux** 
  * `ping`, `netcat`, `ip a`, `ss`
* création d'un **routage statique** simple avec des VMs Linux comme clients et les PCs hôtes comme routeurs
  * tout le monde doit connaître une passerelle vers les réseaux qu'il a besoin de joindre
* aperçu de la **gestion de nom de domaines**

---

# Annexe 1 : Désactiver SELinux
SELinux (*Security Enhanced Linux*) est un outil présent sur les distributions GNU/Linux dérivés de RHEL (comme CentOS).  
**Pour nos TPs, la seule chose à savoir, c'est qu'on va le désactiver :**
* d'abord `sudo setenforce 0`
* puis modifier le fichier `/etc/selinux/config`
  * modifier la valeur de `SELINUX` à `permissive`
  * `SELINUX=permissive`
* pour vérifier : `sestatus` doit afficher `Current mode:   permissive`
