# B1 Réseau 2019 - TP3

# TP 3 - Routage, ARP, Spéléologie réseau 

Pour ce TP :
* que des machines virtuelles
  * CentOS7 sans GUI
* mettez-vous sous la main les mémos 
  * [Réseau CentOS](../../cours/memo/linux_centos_network.md) 
  * et celui des [commandes GNU/Linux](../../cours/memo/linux_commandes.md)

[Les instructions de rendus sont toujours dispos à la racine du répertoire `tp/`](../README.md).

# Sommaire

- [B1 Réseau 2019 - TP3](#b1-r%c3%a9seau-2019---tp3)
- [TP 3 - Routage, ARP, Spéléologie réseau](#tp-3---routage-arp-sp%c3%a9l%c3%a9ologie-r%c3%a9seau)
- [Sommaire](#sommaire)
- [Préparation de l'environnement](#pr%c3%a9paration-de-lenvironnement)
  - [1. Mise à jour du patron](#1-mise-%c3%a0-jour-du-patron)
  - [2. Mise en place du lab](#2-mise-en-place-du-lab)
      - [Réseaux](#r%c3%a9seaux)
      - [Tableau d'adressage](#tableau-dadressage)
      - [Schéma réseau](#sch%c3%a9ma-r%c3%a9seau)
      - [Indications](#indications)
- [I. Mise en place du routage](#i-mise-en-place-du-routage)
  - [1. Configuration du routage sur `router`](#1-configuration-du-routage-sur-router)
  - [2. Ajouter les routes statiques](#2-ajouter-les-routes-statiques)
  - [3. Comprendre le routage](#3-comprendre-le-routage)
- [II. ARP](#ii-arp)
  - [1. Tables ARP](#1-tables-arp)
  - [2. Requêtes ARP](#2-requ%c3%aates-arp)
    - [A. Table ARP 1](#a-table-arp-1)
    - [B. Table ARP 2](#b-table-arp-2)
    - [C. `tcpdump` 1](#c-tcpdump-1)
    - [D. `tcpdump` 2](#d-tcpdump-2)
    - [E. u okay bro ?](#e-u-okay-bro)
- [Entracte : Donner un accès internet aux VMs](#entracte--donner-un-acc%c3%a8s-internet-aux-vms)
- [III. Plus de `tcpdump`](#iii-plus-de-tcpdump)
  - [1. TCP et UDP](#1-tcp-et-udp)
    - [A. Warm-up](#a-warm-up)
    - [B. Analyse de trames](#b-analyse-de-trames)
  - [2. SSH](#2-ssh)
- [Récap](#r%c3%a9cap)
- [IV. Bonus](#iv-bonus)
  - [1. ARP cache poisoning](#1-arp-cache-poisoning)
  - [2. Serveur Web](#2-serveur-web)
  
---

# Préparation de l'environnement

## 1. Mise à jour du patron

Si ce n'est pas déjà fait, modifiez votre patron :
* mise à jour
  * `sudo yum update -y` puis `sudo yum upgrade -y`
* ajout de paquets
  * `tcpdump`, `traceroute`, `nc`, `bind-utils`, `vim`, etc.
* SELinux désactivé (voir [Annexe 1 du TP2](../2/README.md#annexe-1-désactiver-selinux))
* et autres si vous le souhaitez

## 2. Mise en place du lab

> On appelle souvent "lab" un ensemble de machines pour tester des bails.  
  **- "Hey bro, t'as mis en place le lab pour le TP du prof de réseau ?"**  
  **- "Ouais bro.**"

#### Réseaux

| Réseau | Adresse réseau | Type (dans VBox) |
| ------ | -------------- | ---------------- |
| `net1` | 10.3.1.0/24    | Host-only        |
| `net2` | 10.3.2.0/24    | Host-only        |
| NAT    | Auto           | NAT              |

#### Tableau d'adressage

| Machine          | `net1`     | `net2`     | NAT  |
| ---------------- | ---------- | ---------- | ---- |
| `client1.net1.tp3` | 10.3.1.11  | x          | auto |
| `server1.net2.tp3` | x          | 10.3.2.11  | auto |
| `router.tp3`     | 10.3.1.254 | 10.3.2.254 | auto |

#### Schéma réseau

```
client1            router             server1
+----+.11      .254+----+.254      .11+----+
|    +-------------+    +-------------+    |
+----+             +----+             +----+
```

#### Indications

Mettez en place 3 machines virtuelles (sous CentOS7, version minimale, pas de GUI) :
* Serveur SSH fonctionnel qui écoute sur le port `7777/tcp` (voir [la section dédiée du TP2](../2/README.md#a-ssh))
* [Pare-feu activé et configuré](../../cours/memo/linux_centos_network.md#interagir-avec-le-firewall) pour autoriser le SSH
* [Nom de domaine configuré](../../cours/memo/linux_centos_network.md#changer-son-nom-de-domaine)
* [Fichiers `/etc/hosts`](../../cours/memo/linux_centos_network.md#editer-le-fichier-hosts) de toutes les machines configurés
* [Réseaux et adressage des machines](../../cours/memo/linux_centos_network.md#d%c3%a9finir-une-ip-statique)
* Désactivation de la carte NAT
  * **laissez-la activée dans VirtualBox et désactivez-la dans la VM directement**
  * désactivez-la immédiatement
    * commande `ifdown <INTERFACE_NAME>`
  * désactivation après reboot
    * clause `ONBOOT=no` dans le fichier `/etc/sysconfig/network-scripts/ifcfg-<INTERFACE_NAME>`

🌞 **Prouvez que chacun des points de la préparation de l'environnement ci-dessus ont été respectés** :

| Point                                                     | Méthode pour prouver       |
| --------------------------------------------------------- | -------------------------- |
| Carte NAT désactivée                                      | commande `ip a`            |
| Serveur SSH fonctionnel qui écoute sur le port `7777/tcp` | commande `ss`              |
| Pare-feu activé et configuré                              | commande `firewall-cmd`    |
| Nom configuré                                             | commande `hostname`        |
| Fichiers `/etc/hosts` de toutes les machines configurés   | fichier `/etc/hosts`       |
| Réseaux et adressage des machines                         | commandes `ip a` et `ping` |

Pour la dernière étape, les `ping` suivants doivent être fonctionnels :
* `client1` <> `router`
  * depuis `client1`, `ping router` doit marcher
  * depuis `router`, `ping client1` doit marcher
* `server1` <> `router`
  * depuis `server1`, `ping router` doit marcher
  * depuis `router`, `ping server1` doit marcher

# I. Mise en place du routage

Comme vous l'avez peut-être deviné, on va mettre en place du routage, pour que les membres de `net1` puissent joindre `net2`, et réciproquement.

Pour cela, il va être nécessaire de :
* autoriser la machine `router` à "router" des paquets
* [ajouter des routes](../../cours/memo/linux_centos_network.md#ajouter-une-route-statique) sur les machines si nécessaire, en renseignant la passerelle associée

## 1. Configuration du routage sur `router`

Une machine que l'on appelle "routeur" est une machine capable de rediriger des paquets vers une destination connue. Elle sert de passerelle entre plusieurs réseaux.

Pour autoriser une machine CentOS7 à router des paquets :
```
# Temporaire
$ sudo sysctl -w net.ipv4.conf.all.forwarding=1

# Permanent
## Si vous êtes root
$ echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf

## Si vous êtes pas root
$ echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf

# Vérification
$ sudo sysctl net.ipv4.conf.all.forwarding # la valeur doit être à 1
```

🌞 **Effectuez cette commande sur la machine `router`.**

## 2. Ajouter les routes statiques

Une route permet d'indiquer à une machine comment joindre un réseau donné.  

Pour rappel : 
* toute machine directement connectée à un réseau connaît déjà une route vers ce réseau
* toute machine possède une *table de routage* : cette dernière contient la liste des routes connues par cette machine
* `ip route show` ou `ip r s` permet d'afficher la table de routage d'une machine CentOS7

🌞 **[Ajouter les routes nécessaires](../../cours/memo/linux_centos_network.md#ajouter-une-route-statique) pour que...**
* `client1` puisse joindre `net2`
  * vérifier l'ajout de la route avec `ip r s`
* `server1` puisse joindre `net1`
  * vérifier l'ajout de la route avec `ip r s`
* tester le bon fonctionnement
  * simple test avec la commande `ping`
  * vérification du passage par `router` avec la commande `traceroute`

## 3. Comprendre le routage

Ici, on va utiliser [`tcpdump`](../../cours/memo/linux_commandes.md#tcpdump) et Wireshark (ui, encore) et on va aborder la notion d'ARP.  

`tcpdump` et Wireshark sont les outils de référence quand il s'agit de comprendre vraiment ce qu'il se passe sur le réseau. Sorry si vous aimez pas, on va les lancer à tous les TPs ;)

<div align="center"><img src="./pic/meme_tcpdump.jpg" /></div>

---

Lors d'un `ping` de `client1` vers `server1`, le message passe par le `router`. 

Plus précisément :
* le message rentre par l'interface de `router` dans `net1`
* le routeur regarde sa table de routage et trouve vers quelle interface il doit envoyer le paquet
* le message sort par l'interface de `router` dans `net2`

Donc on peut lancer deux fois `tcpdump` :
* sur l'interface `net1` pour voir les trames qui entrent
* sur l'interface `net2` pour voir les trames qui sortent

---

**Pour mettre en évidence et comprendre le routage :**
* sur `router`, lancez un `tcpdump` sur l'interface dans `net1` : on l'appellera ***capture1***
* sur `router`, lancez un deuxième `tcpdump` sur l'interface dans `net2` : on l'appellera ***capture2***
* sur `client1`, faites un `ping` vers `server1`
* arrêtez les deux `tcpdump`

> **Les paquets du `ping` sont les paquets de type ICMP.**

**Choisissez une trame envoyée par `ping`** (peu importe laquelle)
* vous devriez la voir dans les deux captures : elle arrive du `net1` et elle sort vers `net2`
* trouvez :
  * la MAC source, la MAC destination de la trame dans la ***capture1***
  * l'adresse IP source, l'adresse IP de destination du paquet dans la ***capture1***
  * la MAC source, la MAC destination de la trame dans la ***capture2***
  * l'adresse IP source, l'adresse IP de destination du paquet dans la ***capture2***

🌞 **Faites moi un 'tit tableau représentant cette trame choisie :**

|             | MAC src       | MAC dst       | IP src       | IP dst       |
| ----------- | ------------- | ------------- | ------------ | ------------ |
| Dans `net1` (trame qui entre dans `router`) | <mac_address> | <mac_address> | <ip_address> | <ip_address> |
| Dans `net2` (trame qui sort de `router`) | <mac_address> | <mac_address> | <ip_address> | <ip_address> |

# II. ARP

## 1. Tables ARP

> 📚 Il existe [une section dédiée à la gestion de table ARP dans le mémo CentOS7](../../cours/memo/linux_centos_network.md#g%c3%a9rer-sa-table-arp).

🌞 **Affichez la table ARP de chacun des machines et expliquez toutes les lignes**
* avant de les afficher, faites un `ping` depuis `client1` vers `server1`

## 2. Requêtes ARP

Dans cette section, on va observer les échanges ARP effectués automatiquement par les machines lorsqu'elles communiquent.

Pour cela, `client1` enverra des `ping` à `server1` pendant que l'on observera :
* les changements dans les tables ARP
* les trames ARP échangées avec [`tcpdump`](../../cours/memo/linux_commandes.md#tcpdump)

### A. Table ARP 1

* **videz la table ARP de `client1` ET `router`**
* depuis `client1`, envoyez un paquet à `server1` (`ping` par exemple)
* 🌞 mettez en évidence le changement dans la table ARP de `client1`
  * pour rappel, on peut afficher la table ARP d'une machine avec `ip neigh show`

### B. Table ARP 2

* **videz la table ARP de `client1` ET `server1` ET `router`**
* depuis `client1`, envoyez un paquet à `server1` (`ping` par exemple)
* 🌞 mettez en évidence le changement dans la table ARP de `server1`

### C. `tcpdump` 1

* **videz la table ARP de `client1` ET `router`**
* sur `client1`, lancez `tcpdump` pour capturer toutes les trames que `client1` envoie
* sur `client1`, envoyez un paquet à `server1` (`ping` par exemple)
* sur `client1`, coupez `tcpdump`
* 🌞 mettez en évidence **toutes** les trames ARP capturées lors de cet échange, et expliquer chacune d'entre elles

### D. `tcpdump` 2

* **videz la table ARP de `client1` ET `server1` ET `router`**
* sur `server1`, lancez `tcpdump` pour capturer toutes les trames que `client1` envoie
* sur `client1`, envoyez un paquet à `server1` (`ping` par exemple)
* sur `server1`, coupez `tcpdump`
* 🌞 mettez en évidence **toutes** les trames ARP capturées lors de cet échange, et expliquer chacune d'entre elles

### E. u okay bro ?

🌞 Expliquer, en une suite d'étapes claires, toutes les trames ARP échangées lorsque `client1` envoie un `ping` vers `server1`, en traversant la machine `router`. 

# Entracte : Donner un accès internet aux VMs

Ce serait bien quand même si `client1` pouvait avoir un accès à internet quand même nan ? 

🌞 **Permettre un accès WAN (Internet) à `client1`**
* configurez `router`
  * ré-activez la carte NAT **du `router` uniquement**
  * activez le NAT
    * `firewall-cmd --add-masquerade --permanent`
    * `firewall-cmd --reload`
* configurez `client1`
  * ajout d'une route par défaut
    * vérifier en affichant la table de routage
  * [configuration d'un serveur DNS](../../cours/memo/linux_centos_network.md#configurer-lutilisation-dun-serveur-dns)
* vérifiez que vous avez un accès au WAN (internet) avec les commandes :
  * envoi d'un message simple vers un serveur en ligne
    * `$ ping 8.8.8.8`
  * test de la résolution de nom DNS
    * `$ dig <NOM_DUN_SITE_INTERNET>`
  * test d'installation de paquet
    * `$ sudo yum install -y epel-release`
    * `$ sudo yum install -y sl`
  * lancement de la locomotiiiiiive
    * `$ sl`

# III. Plus de `tcpdump`

<div align="center"><img src="./pic/meme_tcpdump_2.png" /></div>

## 1. TCP et UDP

Hello `nc` my old friend.  

Tant qu'on est là, on va intercepter et analyser un peu plus en détails une connexion `nc`.

**`nc` permet d'écouter ou d'établir une connexion arbitraire à un port, en TCP ou en UDP.**    
C'est à dire qu'il n'y a pas de protocole particulier utilisé pour communiquer une fois le tunnel TCP ou UDP établi (pas de HTTP, pas de SSH, rien, quetchi, quedal).  

On va se servir de lui pour mettre en évidence la différence entre TCP et UDP.

### A. Warm-up

**Effectuez une connexion `nc` où le client est `client1` et le serveur `server1`.**
* testez avec le port `9999/tcp`
* testez avec le port `9999/udp`
* n'oubliez pas d'[ouvrir les ports dans le firewall](../../cours/memo/linux_centos_network.md#interagir-avec-le-firewall) afin de les utiliser :)

### B. Analyse de trames

Pendant que `client1` discute avec `server1`, on est toujours d'accord que toutes les trames passent par `router` nan ? Alors on va intercepter le trafic directement depuis `router`.

🌞 **TCP** :
* lancez un `tcpdump` sur le router
* effectuez un `nc` de client vers le serveur, **en TCP**
  * échangez quelques messages
  * coupez la connexion (CTRL + C)
* mettez en évidence le *3-way handshake TCP*
* observez la suite des échanges (*PSH*, *ACK*, etc)
* observez la fin de connexion

🌞 **UDP** :
* lancez un `tcpdump` sur le router
* effectuez un `nc` de client vers le serveur, **en UDP**
  * échangez quelques messages
  * coupez la connexion (CTRL + C)
* mettez en évidence les différences entre TCP et UDP
  * n'hésitez pas à m'appeler si vous doutez :)

## 2. SSH

🌞 Effectuez une connexion SSH depuis `client1` vers `server1`
* utilisez `tcpdump` pour mettre en évidence des paquets échangés par SSH
* trouvez quel protocole utilise SSH : TCP ou UDP ?

# Récap

Donc si on récapitule...
* **une adresse MAC sert à joindre une machine sur le même réseau que nous**
  * si on la connaît pas, on peut faire une requête ARP
* **une adresse IP sert à joindre une machine sur un réseau donné**
  * il faut connaître une route vers ce réseau
  * si la machine de destination est sur le même réseau que nous, il faut connaître sa MAC
  * si la machine de destination est sur un autre réseau que nous, il faut connaître la MAC de la passerelle qui y mène
* **un port sert à joindre un service précis sur une IP donnée**
  * une fois qu'on peut joindre quelqu'un avec son IP, on peut visiter un port précis
  * TCP et UDP présentent des avantages et inconvénients différents

# IV. Bonus

## 1. ARP cache poisoning

**Mettez en place de l'*ARP cache poisoning* ou *ARP spoofing*** :
* avec `scapy` si vous êtes chaud en dév Python
* avec `arping` c'est juste une commande shell, ça se fait en deux secondes :)
  * [tout est là](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)

🌞 Par exemple, créez une deuxième machine client `client2`. Depuis `client2`, faites croire à `client1` qque vous êtes sa passerelle.

MP moi si besoin d'aide.

<div align="center"><img src="./pic/meme_arp_spoof.jpg" /></div>

## 2. Serveur Web

Mettre en place NGINX, un serveur Web, de façon simplifié :
* installer le paquet `nginx`
* ouvrir le port firewall 80/tcp
* lancer le service NGINX `sudo systemctl start nginx`

🌞 **Mettre en place un serveur web NGINX sur `server1`**
* testez le fonctionnement depuis `client1` 
  * avec par exemple une commande `curl`
  * ou un forwarding SSH

MP moi si besoin d'aide.
